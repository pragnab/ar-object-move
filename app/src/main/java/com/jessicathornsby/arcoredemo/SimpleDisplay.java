package com.jessicathornsby.arcoredemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;

public class SimpleDisplay extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MarshMallowPermission permission = new MarshMallowPermission(SimpleDisplay.this);
        if (!permission.checkPermissionForExternalStorage()) {
            permission.requestPermissionForExternalStorage();
        }
        //Uri uri=Uri.parse("android.resource://"+getPackageName()+"/raw/duck");
        //File f=new File( Uri.parse("android.resource://com.jessicathornsby.arcoredemo/" + R.drawable.logo).getPath());
        Uri url = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.duck);
        File f = new File(url.toString());
        System.out.println("test!!!!!!!!!!!! "+url+"");
        System.out.println("test!!!!!!!!!!!! "+f.getAbsolutePath()+"");
        System.out.println("test!!!!!!!!!!!! "+f.getName()+"");
       // File f=new File(uri.getPath());

        if(f.exists())
        {
            System.out.println("file is exists");
        }
        else{
            System.out.println("file is not available");
        }
        Intent sceneViewerIntent = new Intent(Intent.ACTION_VIEW);
        sceneViewerIntent.setData(Uri.parse("https://arvr.google.com/scene-viewer/1.0?file=https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Avocado/glTF/Avocado.gltf"));
        sceneViewerIntent.setPackage("com.google.android.googlequicksearchbox");
        //startActivity(sceneViewerIntent);



    }
}
