package com.jessicathornsby.arcoredemo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Select_Img extends AppCompatActivity {

    TextView img_han3, img_han2, img_han1,img_han4,img_han5,img_han6;
    public static String img = "han1";

    @Override
    protected void onResume() {
        super.onResume();

        // ARCore requires camera permissions to operate. If we did not yet obtain runtime
        // permission on Android M and above, now is a good time to ask the user for it.
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
            CameraPermissionHelper.requestCameraPermission(this);
            return;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.img_select);
        img_han1 = (TextView) findViewById(R.id.img_han1);
        img_han1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Select_Img.this, MainActivity.class);
                img = "han1";
             //   finish();
                startActivity(a);
            }
        });
        img_han2 = (TextView) findViewById(R.id.img_han2);
        img_han2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Select_Img.this, MainActivity.class);
                img = "han2";
              //  finish();
                startActivity(a);
            }
        });
        img_han3 = (TextView) findViewById(R.id.img_han3);
        img_han3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(Select_Img.this, MainActivity.class);
                img = "han3";
                //finish();
                startActivity(a);
            }
        });

    }
}
